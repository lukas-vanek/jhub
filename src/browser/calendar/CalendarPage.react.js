import '../styles/Calendar.scss';
import Component from 'react-pure-render/component';
import Helmet from 'react-helmet';
import React, { PropTypes } from 'react';
import linksMessages from '../../common/app/linksMessages';
import { FormattedHTMLMessage, defineMessages, injectIntl, intlShape } from 'react-intl';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import _ from 'lodash';
import moment from 'moment';

class CalendarPage extends Component {

  static propTypes = {
    intl: intlShape.isRequired,
    loadTests: PropTypes.func.isRequired,
    loadTasks: PropTypes.func.isRequired,
    loadEvents: PropTypes.func.isRequired,
    tests: PropTypes.object.isRequired,
    tasks: PropTypes.object.isRequired,
    events: PropTypes.object.isRequired,
  };

  componentDidMount() {
    const { loadTests, loadTasks, loadEvents } = this.props;
    loadTests();
    loadTasks();
    loadEvents();
  }

  dts(date) {
    return moment(date).format('DDMMYYYY');
  }

  render() {
    const { intl, tests, tasks, events } = this.props;
    const title = intl.formatMessage(linksMessages.calendar);

    const testsDates = tests.map(test => test.deadLine).toArray();
    const tasksDates = tasks.map(task => task.deadLine).toArray();
    const eventsDates = events.map(task => task.date.start).toArray();

    const deadLines = _.uniqBy(
      [...testsDates, ...tasksDates, ...eventsDates],
      one => this.dts(one)
    ).sort(one => -this.dts(one));
    // sorted unique dates for iteration


    return (
      <div className="page-content">
        <Helmet title={title} />

        <table className="calendar-table">
          <thead>
            <tr>
              <td>Datum</td>
              <td>Testy</td>
              <td>Úkoly</td>
              <td>Události</td>
            </tr>
          </thead>
          <tbody>
            {deadLines.map((deadLine, n) =>
              <tr key={n}>

                <td>
                {moment(deadLine).format('D.M.YYYY')}
                </td>

                <td>
                {tests.filter(test =>
                  this.dts(test.deadLine) === this.dts(deadLine)
                ).map((test, i) =>
                  <div className="calendar-item ci-test" key={i}>
                    <div>{test.name}</div>
                    <div className="ci-desc">{test.content}</div>
                  </div>
                )}
                </td>

                <td>
                {tasks.filter(task =>
                  this.dts(task.deadLine) === this.dts(deadLine)
                ).map((task, i) =>
                  <div className="calendar-item ci-task" key={i}>
                    <div>{task.name}</div>
                    <div className="ci-desc">{task.content}</div>
                  </div>
                )}
                </td>

                <td>
                {events.filter(event =>
                  this.dts(event.date.start) === this.dts(deadLine)
                ).map((event, i) =>
                  <div className="calendar-item ci-event" key={i}>
                    <div>{event.name}</div>
                    <div className="ci-desc">{event.description}</div>
                  </div>
                )}
                </td>

              </tr>
            )}
          </tbody>
        </table>

      </div>
    );
  }

}

CalendarPage = injectIntl(CalendarPage);

function mapStateToProps(state, ownProps) {
  return {
    tests: state.tests.map,
    tasks: state.tasks.map,
    events: state.events.map,
    ...ownProps
  };
}

import * as testsActions from '../../common/tests/actions';
import * as tasksActions from '../../common/tasks/actions';
import * as eventsActions from '../../common/events/actions';

const mapDispatchToProps = (dispatch) => bindActionCreators({
  ...testsActions,
  ...tasksActions,
  ...eventsActions,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CalendarPage);
