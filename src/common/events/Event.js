import { Record } from 'immutable';

const Event = Record({
  _id: '',
  name: '', //r
  description: '', //r
  date:{
    start:null, //r
    end:null //r
  },
  place: '', //r
  cost: 0, //r
  
  dateCreate: null, //g
  dateUpdate: null //g
});

export default Event;
