import * as types from '../ActionTypes';
import Event from './Event';
import { Map, Record } from 'immutable';

const InitialState = Record({
  map: Map()
});
const initialState = new InitialState;

export default function eventsReducer(state = initialState, action) {
  if (!(state instanceof InitialState)) return initialState;

  switch (action.type) {

    case "LOAD_EVENTS": {
      const events = action.events.map(event => new Event(event));
      return state.update('map', map => {
        for (let i = 0; i < events.length; i++) {
          if (!map.get(events[i]._id)) {
            map = map.set(events[i]._id, events[i]);
          }
        }
        return map;
      });
    }


  }

  return state;
}
