// import * as types from '../ActionTypes';


export function loadEvents() {
  return {
    type: "LOAD_EVENTS",
    events: [
      {
        _id: "1",
        name: "Event 1",
        description: "Lorem ipsum dolor sit amet",
        date: {
          start: new Date(2016, 5, 1),
          end: new Date(2016, 5, 1)
        }
      },
      {
        _id: "2",
        name: "Event 2",
        description: "Lorem ipsum dolor sit amet",
        date: {
          start: new Date(2016, 5, 2),
          end: new Date(2016, 5, 2)
        }
      },
      {
        _id: "3",
        name: "Event 3",
        description: "Lorem ipsum dolor sit amet",
        date: {
          start: new Date(2016, 5, 5),
          end: new Date(2016, 5, 5)
        }
      },
      {
        _id: "4",
        name: "Event 4",
        description: "Lorem ipsum dolor sit amet",
        date: {
          start: new Date(2016, 5, 8),
          end: new Date(2016, 5, 8)
        }
      }
    ]
  };
}
