import * as types from '../ActionTypes';

export function setCurrentLocale(locale) {
  return {
    type: types.SET_CURRENT_LOCALE,
    payload: { locale }
  };
}
