import { Record } from 'immutable';

const Test = Record({
  _id: '',
  name: '',
  content: '',
  materials: [],
  deadLine: null,
  
  dateCreate: null,
  dateUpdate: null,
});

export default Test;
