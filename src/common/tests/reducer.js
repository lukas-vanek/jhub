import * as types from '../ActionTypes';
import Test from './Test';
import { Map, Record } from 'immutable';

const InitialState = Record({
  map: Map()
});
const initialState = new InitialState;

export default function testsReducer(state = initialState, action) {
  if (!(state instanceof InitialState)) return initialState;

  switch (action.type) {

    case "LOAD_TESTS": {
      const tests = action.tests.map(test => new Test(test));
      return state.update('map', map => {
        for (let i = 0; i < tests.length; i++) {
          if (!map.get(tests[i]._id)) {
            map = map.set(tests[i]._id, tests[i]);
          }
        }
        return map;
      });
    }


  }

  return state;
}
