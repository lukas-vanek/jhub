// import * as types from '../ActionTypes';


export function loadTests() {
  return {
    type: "LOAD_TESTS",
    tests: [
      {
        _id: "1",
        name: "Test 1",
        content: "Lorem ipsum dolor sit amet",
        deadLine: new Date(2016, 5, 1)
      },
      {
        _id: "2",
        name: "Test 2",
        content: "Lorem ipsum dolor sit amet",
        deadLine: new Date(2016, 5, 1)
      },
      {
        _id: "3",
        name: "Test 3",
        content: "Lorem ipsum dolor sit amet",
        deadLine: new Date(2016, 5, 2)
      },
      {
        _id: "4",
        name: "Test 4",
        content: "Lorem ipsum dolor sit amet",
        deadLine: new Date(2016, 5, 3)
      },
      {
        _id: "5",
        name: "Test 5",
        content: "Lorem ipsum dolor sit amet",
        deadLine: new Date(2016, 5, 3)
      },
    ]
  };
}
