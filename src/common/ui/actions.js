import * as types from '../ActionTypes';

export function onSideMenuChange(isOpen) {
  return {
    type: types.ON_SIDE_MENU_CHANGE,
    payload: { isOpen }
  };
}

export function toggleSideMenu() {
  return {
    type: types.TOGGLE_SIDE_MENU
  };
}
