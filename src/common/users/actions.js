// Note prefix ON, it means the action is not dispatched by the viewer.

import * as types from '../ActionTypes';

export function onUsersList(list) {
  return {
    type: types.ON_USERS_LIST,
    payload: { list }
  };
}
