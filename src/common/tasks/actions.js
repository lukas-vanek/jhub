// import * as types from '../ActionTypes';


export function loadTasks() {
  return {
    type: "LOAD_TASKS",
    tasks: [
      {
        _id: "1",
        name: "Task 1",
        content: "Lorem ipsum dolor sit amet",
        deadLine: new Date(2016, 5, 1)
      },
      {
        _id: "2",
        name: "Task 2",
        content: "Lorem ipsum dolor sit amet",
        deadLine: new Date(2016, 5, 2)
      },
      {
        _id: "3",
        name: "Task 3",
        content: "Lorem ipsum dolor sit amet",
        deadLine: new Date(2016, 5, 2)
      },
      {
        _id: "4",
        name: "Task 4",
        content: "Lorem ipsum dolor sit amet",
        deadLine: new Date(2016, 5, 4)
      },
      {
        _id: "5",
        name: "Task 5",
        content: "Lorem ipsum dolor sit amet",
        deadLine: new Date(2016, 5, 5)
      },
      {
        _id: "6",
        name: "Task 6",
        content: "Lorem ipsum dolor sit amet",
        deadLine: new Date(2016, 5, 7)
      },
    ]
  };
}
