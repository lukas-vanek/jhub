import * as types from '../ActionTypes';
import Task from './Task';
import { Map, Record } from 'immutable';

const InitialState = Record({
  map: Map()
});
const initialState = new InitialState;

export default function tasksReducer(state = initialState, action) {
  if (!(state instanceof InitialState)) return initialState;

  switch (action.type) {

    case "LOAD_TASKS": {
      const tasks = action.tasks.map(test => new Task(test));
      return state.update('map', map => {
        for (let i = 0; i < tasks.length; i++) {
          if (!map.get(tasks[i]._id)) {
            map = map.set(tasks[i]._id, tasks[i]);
          }
        }
        return map;
      });
    }


  }

  return state;
}
