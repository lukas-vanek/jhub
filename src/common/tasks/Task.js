import { Record } from 'immutable';

const Task = Record({
  _id: '',
  name: '',
  content: '',
  materials: [],
  deadLine: null,
  
  dateCreate: null,
  dateUpdate: null,
});

export default Task;
