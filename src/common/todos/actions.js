import { Range } from 'immutable';

import * as types from '../ActionTypes';

export function addHundredTodos() {
  // Note how dependency injection ensures pure action.
  return ({ getUid, now }) => {
    const payload = Range(0, 100).map(() => {
      const id = getUid();
      return {
        createdAt: now(),
        id,
        title: `Item #${id}`
      };
    }).toJS();
    return {
      type: types.ADD_HUNDRED_TODOS,
      payload
    };
  };
}

export function addTodo(title) {
  return ({ getUid, now }) => ({
    type: types.ADD_TODO,
    payload: {
      createdAt: now(),
      id: getUid(),
      title: title.trim()
    }
  });
}

export function clearAllCompletedTodos() {
  return {
    type: types.CLEAR_ALL_COMPLETED_TODOS
  };
}

export function clearAllTodos() {
  return {
    type: types.CLEAR_ALL_TODOS
  };
}

export function deleteTodo(id) {
  return {
    type: types.DELETE_TODO,
    payload: { id }
  };
}

export function toggleTodoCompleted(todo) {
  return {
    type: types.TOGGLE_TODO_COMPLETED,
    payload: { todo }
  };
}
