import * as types from '../ActionTypes';
import Todo from './todo';
import { Map, Record } from 'immutable';

const InitialState = Record({
  map: Map()
});
const initialState = new InitialState;

export default function todosReducer(state = initialState, action) {
  if (!(state instanceof InitialState)) return initialState;

  switch (action.type) {

    case types.ADD_HUNDRED_TODOS: {
      const todos = action.payload.reduce((todos, json) =>
        todos.set(json.id, new Todo(json))
      , Map());
      return state.update('map', map => map.merge(todos));
    }

    case types.ADD_TODO: {
      const todo = new Todo(action.payload);
      return state.update('map', map => map.set(todo.id, todo));
    }

    case types.CLEAR_ALL_COMPLETED_TODOS: {
      return state.update('map', map => map.filterNot(todo => todo.completed));
    }

    case types.CLEAR_ALL_TODOS: {
      return state.update('map', map => map.clear());
    }

    case types.DELETE_TODO: {
      const { id } = action.payload;
      return state.update('map', map => map.delete(id));
    }

    case types.TOGGLE_TODO_COMPLETED: {
      const { todo } = action.payload;
      return state.updateIn(['map', todo.id, 'completed'], value => !value);
    }

  }

  return state;
}
