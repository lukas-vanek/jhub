import * as types from '../ActionTypes';
import { Record } from 'immutable';

const InitialState = Record({
  formDisabled: false,
  formError: null
});
const initialState = new InitialState;

export default function authReducer(state = initialState, action) {
  if (!(state instanceof InitialState)) return initialState.mergeDeep(state);

  switch (action.type) {

    case types.LOGIN_START:
      return state.set('formDisabled', true);

  }

  return state;
}
