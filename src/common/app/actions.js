import { setCurrentLocale } from '../intl/actions';

import * as types from '../ActionTypes';

export function updateAppStateFromStorage() {
  return ({ dispatch, engine }) => {
    const getPromise = async () => {
      const state = await engine.load();
      if (state.intl && state.intl.currentLocale) {
        dispatch(setCurrentLocale(state.intl.currentLocale));
      } else if (process.env.IS_SERVERLESS) {
        // TODO: Add a reliable client side only locale detection with failback
        // to config defaultLocale.
        dispatch(setCurrentLocale('en'));
      }
    };
    return {
      type: types.UPDATE_APP_STATE_FROM_STORAGE,
      payload: getPromise()
    };
  };
}
