import bodyParser from 'body-parser';
import cors     from 'cors';
import express  from 'express';
import fs       from 'fs';
import mongoose from 'mongoose';
import passport from 'passport';
import config from './config';
import tools from './tools'

const models_path =  __dirname + '/models'
const api_path = __dirname + '/router'

//Připojí mongoose k MongoDB severu nastavenému v souboru config.
mongoose.connect(config.db);

//Vytvoří hlavbní aplikaci API serveru
var app = express();

//Načte modely ze složky models do mongoose
fs.readdirSync(models_path).forEach( function(file){
  if(file.indexOf('.js'))require(models_path + '/' + file);
});

//Načte middlewary
app.use(cors());
app.use(bodyParser.json());

//načte API routry ze složky api
fs.readdirSync(api_path).forEach( function(file){
  if(file.indexOf('.js')){
    var router = require(api_path + '/' + file);
    app.use('/'+file.split(".")[0],router);
  }
});

app.on('mount', () => {
  console.log('Api is available at %s', app.mountpath);
});

export default app;
