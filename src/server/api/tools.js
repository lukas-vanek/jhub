var tools = {
  //Vrátí chybovou hlášku, pokud string neexistuje, nebo je moc krátký, nebo je moc dlouhý.
  //Validní string vrací false
  invalidString: function(str, min, max){
    if(!str)return "invalidData";
    if(min && str.length<min)return "tooShortString";
    if(max && str.length>max)return "tooLongString";
    return false;
  }
};

export default tools;