var moment = require('moment');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SubjectSchema = new Schema({
  name: String,
  dates: [String],
  teacher: [{type: Schema.ObjectId, ref:'teachers'}],
  homeworks: [{type: Schema.ObjectId, ref:'tasks'}],
  students: [{type: Schema.ObjectId, ref:'students'}],
  
  dateCreate: Date,
  dateUpdate: Date
});

SubjectSchema.pre('save',function(next){
  if(!this.dateCreate) this.dateCreate = moment().utc().toDate();
  this.dateUpdate = moment().utc().toDate();
  next();
});

mongoose.model('subjects',SubjectSchema);