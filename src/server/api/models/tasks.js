var moment = require('moment');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TaskSchema = new Schema({
  name: String,
  content: String,
  materials: [{type: Schema.ObjectId, ref:'materials'}],
  deadLine: Date,
  
  dateCreate: Date,
  dateUpdate: Date,
});

TaskSchema.pre('save',function(next){
  if(!this.dateCreate) this.dateCreate = moment().utc().toDate();
  this.dateUpdate = moment().utc().toDate();
  next();
});

mongoose.model('tasks',TaskSchema);