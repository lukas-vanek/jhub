var moment = require('moment');
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var MaterialSchema = new Schema({
  name: String,
  file: String,
  type: String,
  
  dateCreate: Date,
  dateUpdate: Date,
});

MaterialSchema.pre('save',function(next){
  if(!this.dateCreate) this.dateCreate = moment().utc().toDate();
  this.dateUpdate = moment().utc().toDate();
  next();
});

mongoose.model('materials',MaterialSchema);