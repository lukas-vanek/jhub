var moment = require('moment');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ClassSchema = new Schema({
  name: String, // r  
  students: [{type: Schema.ObjectId, ref:'students'}],
  events: [{type: Schema.ObjectId, ref:'events'}],
  subjects: [{type: Schema.ObjectId, ref:'subjects'}],
  
  dateCreate: Date,
  dateUpdate: Date,
});

ClassSchema.pre('save',function(next){
  if(!this.dateCreate) this.dateCreate = moment().utc().toDate();
  this.dateUpdate = moment().utc().toDate();
  next();
});
mongoose.model('classes',ClassSchema);
