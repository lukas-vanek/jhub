var moment = require('moment');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
	status: String, //g 'student','teacher'
	name: {
		first: String,//r
		last: String//r
	},
	birthDate: Date,//r
	login:{
		username: String,//g
		prefix: String,//g
		password: String//r
		//password2 //r
	},
	classes: {type: Schema.ObjectId, ref:'classes'},//a
	subjects: [{type: Schema.ObjectId, ref:'subjects'}],//a
	events: [{type: Schema.ObjectId, ref:'events'}],//a
	
	dateCreate: Date,//g
	dateUpdate: Date,//g
});

UserSchema.pre('save',function(next){
	if(!this.dateCreate) this.dateCreate = moment().utc().toDate();
	this.dateUpdate = moment().utc().toDate();
	next();
});

mongoose.model('users',UserSchema);