var moment = require('moment');
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var EventSchema = new Schema({
  name: String, //r
  description: String, //r
  date:{
    start:Date, //r
    end:Date //r
  },
  place: String, //r
  cost: Number, //r
  
  dateCreate: Date, //g
  dateUpdate: Date //g
});

EventSchema.pre('save',function(next){
  if(!this.dateCreate) this.dateCreate = moment().utc().toDate();
  this.dateUpdate = moment().utc().toDate();
  next();
});

mongoose.model('events',EventSchema);