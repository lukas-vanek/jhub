var moment = require('moment');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TestSchema = new Schema({
  name: String,
  content: String,
  materials: [{type: Schema.ObjectId, ref:'materials'}],
  deadLine: Date,
  
  dateCreate: Date,
  dateUpdate: Date,
});

TestSchema.pre('save',function(next){
  if(!this.dateCreate) this.dateCreate = moment().utc().toDate();
  this.dateUpdate = moment().utc().toDate();
  next();
});

mongoose.model('exams',TestSchema);