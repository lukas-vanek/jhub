import mongoose from 'mongoose';
import express from 'express';
import tools from '../tools';
var router = express.Router();
var Test = mongoose.model('exams');
 //validace úkolu
function verifyTest(test, done){
  if(tools.invalidString(test.name,3,null))
    return done(400,'invalidName3',null);
  if(!test.deadLine)
    return done(400,'invalidDeadLine');
  done(null, 'success', test);
}

router.route('/new').post(function(req,res){
  var test = req.body;
  verifyTest(test, function(status, err,o){
    if(err)
      return res.status(status).send(err).end();
    test.create(o, function(err,o){
      if(err)
        return res.status(500).send('errorDb').end();
      res.status(200).send('success').end();
    });    
  });
});

router.route('/id/:id').get(function(req,res){
  Test.findById(req.params.id, function(err,o){
    if(err)
      return res.status(500).send('errorDb').end();
    if(!o)res.status(404).send('notFound').end();
    res.json(o).end();
  });
});

router.route('/all').get(function(req,res){
  Test.find({}, function(err,o){
    if(err)
      return res.status(500).send('errorDb');
    res.json(o).end();
  }); 
});

export default router;