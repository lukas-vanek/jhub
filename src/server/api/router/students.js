import mongoose from 'mongoose';
import express from 'express';
import tools from '../tools';
var router = express.Router();
var User = mongoose.model('users');

function verifyUser(user, done){
    if(tools.invalidString(user.name.first,3,null))return done(400,"Jméno musí mít aspoň 2 písmena.",null);
    if(tools.invalidString(user.name.last,2,null))return done(400,"Příjmení musí mít aspoň 2 písmena.",null);
    if(!user.birthDate)return done(400,"Vyplňte datum narození.", null);
    if(tools.invalidString(user.login.password,5,null))return done(400,"Heslo musí mít aspoň 5 písmen.", null);
    if(user.login.password!=user.login.password2)return done(400,"Hesla nejsou totožná.", null);
    User.find({'login.prefix': user.login.prefix},function(err,arr){
       if(err)done(500,"Error na straně databáze.", null);
       user.login.username = user.login.prefix+("0" + arr.length).slice(-2);
       done(200,null, user);
    });
}

router.route('/new').post(function(req,res){
    var user = req.body;
    user.status = 'student';
    user.login.prefix = "x"+(user.name.last+user.name.first).substr(0,5);
    verifyUser(user, function(status, err,o){
        if(err)return res.status(status).send(err).end();
        User.create(o, function(err,o){
            if(err)return res.status(500).send("Error na straně databáze.").end();
            res.status(200).send("Student úspěšně vytvořen.").end();
        });    
    });
});

router.route('/id/:id').get(function(req,res){
    User.findById(req.params.id, function(err,o){
       if(err)return res.status(500).send("Error na straně databáze.").end();
       if(o.status!='student')res.status(404).send("Hledaný student nenalezen.").end();
       res.json(o).end();
    }); 
});

router.route('/all').get(function(req,res){
    User.find({}, function(err,arr){
       if(err)return res.status(500).send("Error na straně databáze.");
       res.json(arr).end();
    });
});

export default router;