import mongoose from 'mongoose';
import express from 'express';
import tools from '../tools';
var router = express.Router();
var Subject = mongoose.model('subjects');

function verifySubject(subject, done)
{
  if (tools.invalidString(subject.name,1,null))
    return done(400,'invalidName1',null);
  if (!(subject.dates instanceof Array) || subject.dates.length === 0)
    return done(400,'invalidDates',null);
}

router.route('/new').post(function(req,res)
{
  var subject = req.body;
  verifySubject(subject, function(status, err,o){
    if(err)
      return res.status(status).send(err).end();
    subject.create(o, function(err,o){
      if(err)
        return res.status(500).send('errorDb').end();
      res.status(200).send('success').end();
    });    
  });
});

router.route('/id/:id').get(function(req,res){
  Subject.findById(req.params.id, function(err,o){
    if(err)
      return res.status(500).send('errorDb').end();
    res.json(o).end();
  }); 
});

router.route('/all').get(function(req,res){
  Subject.find({}, function(err,arr){
    if(err)
      return res.status(500).send('errorDb');
    res.json(arr).end();
  });
});

export default router;
