import mongoose from 'mongoose';
import express from 'express';
import tools from '../tools';
var router = express.Router();
var Event = mongoose.model('events');

    //validace eventu
function verifyEvent(event, done){
    if(tools.invalidString(event.name,5,null))return done(400,'invalidName5',null);
    if(!event.description)return done(400,'invalidDescription', null);
    if(!event.date)return done(400,'invalidDate', null);
    if(!event.place)return done(400,'invalidPlace', null);
    done(null, 'success', event);
}

router.route('/new').post(function(req,res){
    var event = req.body;
    verifyEvent(event, function(status,err,o){
        if(err)
            return res.status(status).send(err).end();
        Event.create(o, function(err,o){
            if(err)
                return res.status(500).send('errorDb').end();
            res.status(200).send('success').end();
        });    
    });
});

router.route('/id/:id').get(function(req,res){
    Event.findById(req.params.id, function(err,o){
        if(err)
            return res.status(500).send('errorDb').end();
        if(!o)res.status(404).send('notFound').end();
        res.json(o).end();
    }); 
});

router.route('/all').get(function(req,res){
    Event.find({}, function(err,o){
        if(err)
            return res.status(500).send('errorDb');
        res.json(o).end();
    }); 
});

export default router;