import mongoose from 'mongoose';
import express from 'express';
import tools from '../tools';
var router = express.Router();
var Task = mongoose.model('tasks');
 //validace úkolu
function verifyTask(task, done){
	if(tools.invalidString(task.name,3,null)) return done(400,'invalidName3',null);
	if(!task.deadLine) return done(400,'invalidDeadLine');
	done(null, 'success', task);
}

router.route('/new').post(function(req,res){
    var task = req.body;
    verifyTask(task, function(status, err,o){
        if(err)return res.status(status).send(err).end();
        Task.create(o, function(err,o){
            if(err)return res.status(500).send('errorDb').end();
            res.status(200).send('success').end();
        });    
    });
});

router.route('/id/:id').get(function(req,res){
    Task.findById(req.params.id, function(err,o){
       if(err)return res.status(500).send('errorDb').end();
       if(!o)res.status(404).send('notFound').end();
       res.json(o).end();
    }); 
});

router.route('/all').get(function(req,res){
    Task.find({}, function(err,o){
       if(err)return res.status(500).send('errorDb');
       res.json(o).end();
    }); 
});
//scnscdfhcdjhfjb
export default router;